# Project Overview:
RestAssured TestNG Framework stands as a robust API testing framework leveraging Rest Assured at its core. Engineered to streamline and enrich the API testing journey, it offers a seamless and intuitive framework. Our primary objective is to enhance reliability and foster smooth integration into existing workflows, thereby promoting collaboration between developers and testers.

## Technology Stack:
- **Rest Assured**: The fundamental API testing library.
- **Java**: Core programming language for project development.
- **Maven**: Dependency management and build tool.

# Setup Instructions:
Embark on your testing journey with the following setup instructions:

1. **Project Initialization**:
   - Create a new Java project.

2. **API Configuration**:
   - Configure all necessary REST APIs.
   - Execute API calls.
   - Utilize RestAssured for handling requests and responses.
   - Leverage JsonPath for response parsing.

3. **Validation**:
   - Employ TestNG Library for thorough response validation.

4. **Data Handling**:
   - Read data from an Excel file using ApachePOI.

# Key Features:
Experience the power of the framework with the following features:

- **HTTP Method Automation**: Automate various REST API methods including POST, PUT, GET, PATCH, and DELETE.
  
# Execution and Reporting:
Follow these steps to execute tests and generate user-friendly reports:

1. **Project Setup**:
   - Organize your project files in a designated folder.

2. **Version Control**:
   - Initialize Git repository within the project folder.

3. **Execution**:
   - Run your tests.

4. **Report Generation**:
   - Utilize built-in features to generate comprehensive reports.

5. **Git Integration**:
   - Commit and push your project to Git Bash for seamless collaboration and version control.

This framework empowers you to conduct efficient and comprehensive API testing, ensuring the robustness and reliability of your applications.