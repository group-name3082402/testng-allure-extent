Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Thu, 07 Mar 2024 08:37:51 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"628","createdAt":"2024-03-07T08:37:51.623Z"}