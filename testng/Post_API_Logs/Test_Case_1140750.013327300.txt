Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Thu, 07 Mar 2024 08:37:50 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"346","createdAt":"2024-03-07T08:37:50.462Z"}