Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Thu, 07 Mar 2024 09:06:52 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"938","createdAt":"2024-03-07T09:06:52.367Z"}