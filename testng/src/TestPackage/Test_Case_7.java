package TestPackage;

import java.io.File;
import java.io.IOException;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.Environment;
import Repository.RequestBody;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Test_Case_7 extends RequestBody {

	String requestBody;
	String Endpoint;
	File dir_name;
	Response response;
	int statuscode = 0;

	@BeforeTest
	public void setup() throws ClassNotFoundException, IOException {

		dir_name = Utility.CreateLogDirectory("Post_API_Logs");
		requestBody = RequestBody.req_post_tc("Post_TC5");
		String Endpoint = Environment.Hostname() + Environment.Resource_login();
	}

	@Test(dataProvider = "body_nologin", dataProviderClass = Repository.RequestBody.class, description = "post_login_test")
	public void validator(String Req_name, String Req_job) throws IOException {
		String Endpoint = Environment.Hostname() + Environment.Resource_login();
		requestBody = "{\r\n" + "    \"email\": \"" + Req_name + "\",\r\n" + "    \"password\": \"" + Req_job + "\"\r\n"
				+ "}";
		
		System.out.println(RequestBody.HeaderName());
		System.out.println(RequestBody.HeaderValue());
		System.out.println(requestBody);
		System.out.println(Endpoint);
		response = API_Trigger.Post_trigger(RequestBody.HeaderName(), RequestBody.HeaderValue(), requestBody, Endpoint);
		ResponseBody res_body = response.getBody();
		statuscode = response.statusCode();
		System.out.println(statuscode);
		
		// Assertions
		Assert.assertEquals(statuscode, 400);
		Utility.evidenceFileCreator(Utility.testLogName("Post_nologin"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());
		System.out.println(res_body);
		Assert.assertEquals(response.getBody().asString(), "{\"error\":\"Missing password\"}");

	}

	@AfterTest
	public void evidenceCreator() {
	}
}
